--/*
--Return tables that match a specified table name
SELECT information_schema.columns.table_name, information_schema.columns.column_name
FROM information_schema.columns
WHERE information_schema.columns.table_name ILIKE '%service_attempts%'
ORDER BY 1, 2
;
--*/
/*
--Return tables that contain a specified column name
SELECT information_schema.columns.column_name, information_schema.columns.table_name
FROM information_schema.columns
WHERE information_schema.columns.column_name ILIKE '%user_id%'
ORDER BY 1
;
--*/